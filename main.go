package main

import (
	"encoding/csv"
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	//"golang.org/x/net/html/charset"
	"io"
	"io/ioutil"
	"log"
	//"net/http"
	"strconv"
	"strings"
	"time"
)

const urlTemplate = "http://www.zakupki.gov.ru/epz/bankguarantee/extendedsearch/csv.html?searchString=&morphology=on&pageNumber=1&sortDirection=false&recordsPerPage=_10&sortBy=PUBLISH_DATE_SORT&published=on&rejected=on&terminated=on&stopped=on&application=on&contract=on&bgNumberAssignCreditInst=&publishDateFrom=&publishDateTo=&updateDateFrom=&updateDateTo=&validityStartDateFrom=&validityEndDateTo=&issueDateFrom=%s&issueDateTo=%s&priceFrom=%d&priceTo=%d&currencyId=-1&supplier=&bankSearchItemTitle=&bankSearchItemCode=&bankSearchItemFz94id=&bankSearchItemFz223id=&bankSearchItemInn=&customerTitle=&customerCode=&customerFz94id=&customerFz223id=&customerInn=&contractNumber=&orderNumber="

var db *gorm.DB

func main() {
	var err error
	db, err = gorm.Open("mysql", "root:root@/goszakupki?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	//db.Set("gorm:table_options", "ENGINE=InnoDB").CreateTable(&BG{})

	/*bytes, _ := ioutil.ReadFile("jan_2016.csv")
	csvArray := parseCsv(string(bytes))
	fmt.Println(len(csvArray))
	for _, arr := range csvArray {
		if bg, err := NewBGFromArray(arr); err == nil {
			db.Create(bg)
			//fmt.Printf("%v\n\n", bg)
		} else {
			fmt.Println("ERR: ", err)
		}
	}*/
	fmt.Println("Start")
	date := time.Date(2015, time.May, 16, 1, 0, 0, 0, time.UTC)
	for i := 1; i <= 1; i++ {
		date = date.Add(24 * time.Hour)
		if err := fecthDateAndWriteToFile(date, 5000, 20000000, "jan_2016_2.csv"); err != nil {
			fmt.Println("ERROR: ", err)
		}
	}

}

//BG - струкутра описывающая банковскую гарантию
type BG struct {
	ID             uint
	RegNumber      string
	CreditNumber   string
	IssueDate      time.Time
	AddingDate     string
	ApdatingDate   string
	Summ           float64
	Currency       string
	ProviderName   string
	ProviderInn    string
	BankName       string
	BankInn        string
	CustomerName   string
	CustomerInn    string
	SupportType    string
	ContractNumber string
	PurchaseNumber string
	StartDate      string
	EndDate        string
	Status         string
}

//NewBGFromArray - создает новую гарантию на основе массива из CSV
func NewBGFromArray(in []string) (*BG, error) {
	if len(in) != 19 {
		return nil, errors.New("Массив должен содержать 19 элементов")
	}

	summ, err := strconv.ParseFloat(in[5], 32)
	if err != nil {
		return nil, err
	}

	issueDate, err := time.Parse("02.01.2006", in[2])
	if err != nil {
		return nil, err
	}

	resBg := BG{
		RegNumber:      in[0],
		CreditNumber:   in[1],
		IssueDate:      issueDate,
		AddingDate:     in[3],
		ApdatingDate:   in[4],
		Summ:           summ,
		Currency:       in[6],
		ProviderName:   in[7],
		ProviderInn:    in[8],
		BankName:       in[9],
		BankInn:        in[10],
		CustomerName:   in[11],
		CustomerInn:    in[12],
		SupportType:    in[13],
		ContractNumber: in[14],
		PurchaseNumber: in[15],
		StartDate:      in[16],
		EndDate:        in[17],
		Status:         in[18],
	}

	return &resBg, nil
}

func parseCsv(in string) [][]string {
	trimArray := func(in []string) {
		for i, item := range in {
			in[i] = strings.Trim(item, "'")
		}
	}

	r := csv.NewReader(strings.NewReader(in))
	r.Comma = ';'
	result := [][]string{}
	i := 0
	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Println("ERR: ", err)
		}
		if i > 0 {
			trimArray(record)
			result = append(result, record)
		}

		i++
	}

	return result
}

func fecthDateAndWriteToFile(date time.Time, priceFrom int, priceTo int, fileName string) error {
	csvArray, err := getCsv(date.Format("02.01.2006"), date.Format("02.01.2006"), priceFrom, priceTo)
	if err != nil {
		return err
	}
	fmt.Println("LINES ", len(csvArray))

	for _, arr := range csvArray {
		if bg, err := NewBGFromArray(arr); err == nil {
			db.Create(bg)
			//fmt.Printf("%v\n\n", bg)
		} else {
			fmt.Println("ERR: ", err)
		}
	}

	return nil
}

func getCsv(issueDateFrom string, issueDateTo string, priceFrom int, priceTo int) ([][]string, error) {
	fmt.Printf("For date [%s to %s] and price [%d to %d]\n", issueDateFrom, issueDateTo, priceFrom, priceTo)
	resultCsv := [][]string{}
	url := fmt.Sprintf(urlTemplate, issueDateFrom, issueDateTo, priceFrom, priceTo)

	body, err := doRequest(url)
	if err == nil {
		resultCsv = parseCsv(string(body))
	}

	if len(resultCsv) >= 500 {
		deltaPrice := priceTo - priceFrom
		middlePrice := int(deltaPrice / 2)

		chank1, errCh1 := getCsv(issueDateFrom, issueDateTo, priceFrom, middlePrice)
		if errCh1 != nil {
			return [][]string{}, errCh1
		}

		chank2, errCh2 := getCsv(issueDateFrom, issueDateTo, middlePrice+1, priceTo)
		if err != nil {
			return [][]string{}, errCh2
		}

		resultCsv = append(chank1, chank2...)

	}

	return resultCsv, nil
}

func doRequest(url string) ([]byte, error) {
	return ioutil.ReadFile("jan_2016.csv")

	/*resp, err := http.Get(url)
	if err != nil {
		return []byte{}, err
	}

	if resp.StatusCode != 200 {
		fmt.Println("RESPONSE: ", resp.Status)
		return []byte{}, nil
	}

	defer resp.Body.Close()

	utf8, err := charset.NewReader(resp.Body, resp.Header.Get("Content-Type"))
	if err != nil {
		return []byte{}, err
	}

	body, err := ioutil.ReadAll(utf8)
	//fmt.Println("RESULT: ", string(body))
	return body, err*/
}
